<?php
session_start();
$config = parse_ini_file(realpath(__DIR__ . DIRECTORY_SEPARATOR . "../config.ini"), true );

$secret = $config['authentication']['secret'];
$username = $_POST['username'];
$password = $_POST['password'];

if(! $secret){
    $error = "Bad+Config";
}elseif(! $username){
    $error = "Wrong+Username";
}elseif($_POST['password'] != $secret){
    $error = "Wrong+Password";
}else{
    $_SESSION['auth'] = 1;
    $_SESSION['username'] = $_POST['username'];
    header("Location: /index.php");
}
if($error){
    header("Location: /signin.php?error=" .  $error);
}