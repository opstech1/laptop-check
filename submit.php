<?php
require(realpath(__DIR__ . DIRECTORY_SEPARATOR . 'auth/auth_check.php'));
require(realpath(__DIR__ . DIRECTORY_SEPARATOR . 'db/db_connect.php'));

$asset = $_POST['asset'];
$student = $_POST['student'];
$missing_charger = $_POST['missing_charger'] ? 1 : 0;
$needs_repair_keyboard = $_POST['needs_repair_keyboard'] ? 1 : 0;
$needs_repair_display = $_POST['needs_repair_display'] ? 1 : 0;
$needs_repair_hinge = $_POST['needs_repair_hinge'] ? 1 : 0;
$needs_repair_other = $_POST['needs_repair_other'] ? 1 : 0;

$query = "INSERT INTO `laptop_check`.`laptop_check_2020` (`StudentID`, `AssetTag`, `MissingCharger`, `NeedsRepair_keyboard`, `NeedsRepair_display`, `NeedsRepair_hinge`,`NeedsRepair_other`) VALUES (:student_id, :asset, :missing_charger, :needs_repair_keyboard, :needs_repair_display, :needs_repair_hinge, :needs_repair_other);";

$stmt = $conn->prepare($query);
$stmt->bindValue(':student_id', $student, PDO::PARAM_STR);
$stmt->bindValue(':asset', $asset, PDO::PARAM_STR);
$stmt->bindValue(':missing_charger', $missing_charger, PDO::PARAM_INT);

$stmt->bindValue(':needs_repair_keyboard', $needs_repair_keyboard, PDO::PARAM_INT);
$stmt->bindValue(':needs_repair_display', $needs_repair_display, PDO::PARAM_INT);
$stmt->bindValue(':needs_repair_hinge', $needs_repair_hinge, PDO::PARAM_INT);
$stmt->bindValue(':needs_repair_other', $needs_repair_other, PDO::PARAM_INT);

$success = $stmt->execute();

header('Content-type:application/json;charset=utf-8');
if(! $success){
    echo json_encode( array('status' => 'error', 'error_code' => $stmt->errorCode(), 'error_info' => $stmt->errorInfo()) );
    die();
}else{
    echo json_encode( array('status' => 'success') );
}


