<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
  <a class="navbar-brand" href="#">OPS Laptop Check</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                
            </li>
            <li class="nav-item">
                <div class="row">
                    <p class="text-white text-center mt-2 mr-3" style="font-size: 1.2em">
                        USER: <?php print_r($_SESSION['username']) ?>
                    </p>
                    <a type='button' href="auth/logout.php" class="btn btn-secondary btn-lg">Logout</a>
                </div>
            </li>
        </ul>
  </div>
</nav>

