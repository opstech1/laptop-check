
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.80.0">
    <title>OPS Laptop Check - Signin</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.6/examples/sign-in/">

    

    <!-- Bootstrap core CSS -->
    <?php include('lib/css/css_include.php'); ?>

        <!-- Favicons -->
    <meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="lib/css/signin.css" rel="stylesheet">
  
    <!-- JavaScript Bundle with Popper -->
    <?php include('lib/js/js_include.php'); ?>

  </head>
  <body class="text-center">
    
    <form class="form-signin" action="./auth/authenticate.php" method="POST">
        <img class="mb-4" src="/docs/4.6/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>

        <?php
          if($_GET['error']){
            if($_GET['error'] == 'Wrong Password'){
              $error_message = 'Wrong Password';
            }else{
              $error_message = 'An unknown error has occured';
            }
        ?>

<div class="alert alert-danger alert-dismissible fade show" role="alert">
          
        <?php
          print_r($error_message);
        ?>
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
        <?php
          }
        
        ?>
        <div class="form-group row">
          <label for="inputUsername" class="col-sm-3 col-form-label col-form-label-lg">Username:</label>
          <div class="col-sm-9">
            <input name="username" type="text" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
          </div>
        </div>
        
        <div class="form-group row">
          <label for="inputPassword" class="col-sm-3 col-form-label col-form-label-lg">Password:</label>
          <div class="col-sm-9">
            <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
          </div>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        <p class="mt-5 mb-3 text-muted">&copy; 2020-2021</p>
    </form>
</body>
</html>
