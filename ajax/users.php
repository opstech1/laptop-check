<?php
require(realpath(__DIR__ . DIRECTORY_SEPARATOR . '../auth/auth_check.php'));
require(realpath(__DIR__ . DIRECTORY_SEPARATOR . '../db/db_connect.php'));

if(! $_GET['term']){
    exit;
}

$search = trim($_GET['term']);
$list = explode(",", $search);
$lastname = trim($list[0]);
$firstname = trim($list[1]);

$query = "SELECT * FROM users 
WHERE ID = :search 
OR (
    ID != :search AND 
        (
            1=0
            OR username LIKE CONCAT('%', :search, '%')
            OR email LIKE CONCAT('%', :search, '%')
            OR ( lastname LIKE CONCAT('%', :lastname, '%') AND firstname LIKE CONCAT('%', :firstname, '%') )
        )
    )
ORDER BY grade, lastname, firstname";

$stmt = $conn->prepare($query);
$stmt->bindValue(':search', $search, PDO::PARAM_STR);
$stmt->bindValue(':lastname', $lastname, PDO::PARAM_STR);
$stmt->bindValue(':firstname', $firstname, PDO::PARAM_STR);

$success = $stmt->execute();

if(! $success){
    echo json_encode( array('error' => $stmt->errorInfo()) );
    die();
}
$result = $stmt->fetchAll();

header('Content-type:application/json;charset=utf-8');

array_walk($result, function(&$value, &$key){
    $text = $value['lastname'] . ', ' . $value['firstname'] . ' - ' . $value['grade'];
    $value = array('id' => $value['ID'], 'text' => $text);
});
echo json_encode( array('results' => $result) );