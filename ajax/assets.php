<?php
require(realpath(__DIR__ . DIRECTORY_SEPARATOR . '../auth/auth_check.php'));
require(realpath(__DIR__ . DIRECTORY_SEPARATOR . '../db/db_connect.php'));

if(! $_GET['term']){
    exit;
}

$search = $_GET['term'];

$query = "SELECT * FROM assets 
WHERE asset = :search 
OR (asset != :search AND serial LIKE CONCAT('%', :search, '%') )
OR (asset != :search AND asset LIKE CONCAT('%', :search, '%') )
ORDER BY asset";

$stmt = $conn->prepare($query);
$stmt->bindValue(':search', $search, PDO::PARAM_STR);
$success = $stmt->execute();

if(! $success){
    echo json_encode( array('error' => $stmt->errorInfo()) );
    die();
}
$result = $stmt->fetchAll();

header('Content-type:application/json;charset=utf-8');

array_walk($result, function(&$value, &$key){
    $text = $value['asset'] . ' (' . $value['serial'] . ')';
    $value = array('id' => $value['asset'], 'text' => $text);
});
echo json_encode( array('results' => $result) );