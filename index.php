<?php
require(realpath(__DIR__ . DIRECTORY_SEPARATOR . 'auth/auth_check.php'));
require(realpath(__DIR__ . DIRECTORY_SEPARATOR . 'db/db_connect.php'));

?>

<html>

<head>
    <?php include('lib/css/css_include.php'); ?>
    <?php include('lib/js/js_include.php'); ?>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <style>
    /* override the width size for select2 to prevent overflowing the container */
    .select2 {
        width: 100% !important;
    }

    /* checkbox sizes */
    .checkbox-lg .custom-control-label::before, 
    .checkbox-lg .custom-control-label::after {
        top: .8rem;
        width: 1.55rem;
        height: 1.55rem;
    }

    .checkbox-lg .custom-control-label {
        padding-top: 13px;
        padding-left: 6px;
    }


    .checkbox-xl .custom-control-label::before, 
    .checkbox-xl .custom-control-label::after {
        top: 1.2rem;
        width: 1.85rem;
        height: 1.85rem;
    }

    .checkbox-xl .custom-control-label {
        padding-top: 23px;
        padding-left: 10px;
    }
    </style>
    <script>
    $(document).ready(function() {

        var assetSelect = $('#asset').select2({
            ajax: {
                url: 'ajax/assets.php',
                delay: 250,
                dataType: 'json',

                processResults: function (data) {                    
                    var searchTerm = assetSelect.data("select2").$dropdown.find("input").val();
                    if (data.results.length == 1 && data.results[0].id == searchTerm) {
                        assetSelect.append($("<option />")
                            .attr("value", data.results[0].id)
                            .html(data.results[0].text)
                        ).val(data.results[0].id).trigger("change").select2('close').trigger('select2:select');
                    }
                    return data;
                },
            },
            theme: 'bootstrap4',
            placeholder: 'L######',
            minimumInputLength: 3,
            dropdownParent: $('div[name=asset_search]'),
            
        }).focus()
        .select2('open')
        .on('select2:select', function(e) {
            $('#student').focus().select2('open');
        });

        var studentSelect = $('#student').select2({
            ajax: {
                url: 'ajax/users.php',
                delay: 250,
                dataType: 'json',
                processResults: function (data) {                    
                    var searchTerm = studentSelect.data("select2").$dropdown.find("input").val();
                    if (data.results.length == 1 && data.results[0].id == searchTerm) {
                        studentSelect.append($("<option />")
                            .attr("value", data.results[0].id)
                            .html(data.results[0].text)
                        ).val(data.results[0].id).trigger("change").select2('close').trigger('select2:select');
                    }
                    return data;
                },
            },
            theme: 'bootstrap4',
            placeholder: "ID, Username, or Email",
            minimumInputLength: 3,
            dropdownParent: $('div[name=student_search]'),
        })
        .on('select2:select', function(e) {
            $('#needs_repair').focus();
        });

        $(document).on('submit', 'form#check_form', function(e){
            var form = $('form#check_form')
            var form_inputs = $(this).find('select, input')
            var submit_button = $('form').find('[type=submit]')
            // Disable submit button
            submit_button.attr("disabled", true)

            // Perform post
            $.ajax({
                method: "POST",
                url: "submit.php",
                data: form.serialize(),
            }).done(function(data){
                if(data.status == 'error'){
                    // Splash fail
                    CreateSplash('alert-danger', 'Error saving record. Error Code: ' + data.error_code);
                }else if(data.status == 'success'){
                    // Splash success
                    let splash = CreateSplash('alert-success', 'Record saved!');
                    $(splash).delay(1500).fadeOut();
                    // Clear fields
                    form.find('input[type=checkbox]').prop( "checked", false)
                    form.find('select').val(null).trigger('change')
                    $('#asset').select2('open').trigger('select2:open')
                }else{
                    CreateSplash('alert-danger', 'The response from the server is invalid.');
                }
            }).fail(function(){
                // Splash unknown error
                CreateSplash('alert-danger', 'An unknown error occurred.');
            }).always(function(e){
                // Unlock Submit Button
                submit_button.attr("disabled", false)
            });

            // Prevent Defaults
            e.preventDefault();
            e.stopPropagation();
        });

        function CreateSplash(class_name, message){
            $splash = $('\
                <div name="splash" class="alert alert-dismissible fade show" role="alert">\
                    <p id="splash_message">'+ message +'</p>\
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
                        <span aria-hidden="true">&times;</span>\
                    </button>\
                </div>');
            $splash.addClass(class_name)
            $('#splash_area').append($splash)
            return $splash
        }
        
    });

    /*
     * Hacky fix for a bug in select2 with jQuery 3.6.0's new nested-focus "protection"
     * see: https://github.com/select2/select2/issues/5993
     * see: https://github.com/jquery/jquery/issues/4382
     *
     * TODO: Recheck with the select2 GH issue and remove once this is fixed on their side
     */

    $(document).on('select2:open', (e) => {
        var id = e.target.id
        $(document.querySelector('div[name='+id+'_search]')).find('input').get(0).focus()
    });
    </script>
</head>

<body>
    <?php include('lib/navbar.php'); ?>

    <main role="main" class="container">
        <div class="jumbotron font-weight-bold">

            <div id="splash_area">
            </div>
            <form id='check_form' class="needs-validation">
                <div class="form-group pb-5">
                    <label for="asset">Asset:</label>
                    <select name="asset" class="form-control" id="asset" required>
                    </select>
                    <div name='asset_search'>
                    </div>
                </div>
                <div class="form-group pt-3 pb-5">
                    <label for="student">Student:</label>
                    <select name="student" class="form-control" id="student" required>
                    </select>
                    <div name='student_search'>
                    </div>
                </div>

                <div class='pt-4'>
                    <h4>
                        Missing:
                    </h4>
                </div>

                <div class="custom-control custom-checkbox checkbox-xl">
                    <input name="missing_charger" type="checkbox" class="custom-control-input" id="missing_charger">
                    <label class="custom-control-label" for="missing_charger">Charger</label>
                </div>
                
                <div class='pt-4'>
                    <h4>
                        Damage:
                    </h4>
                </div>
                <div class="custom-control custom-checkbox checkbox-xl">
                    <input name="needs_repair_keyboard" type="checkbox" class="custom-control-input" id="needs_repair_keyboard">
                    <label class="custom-control-label" for="needs_repair_keyboard">Keyboard / Trackpad</label>
                </div>

                <div class="custom-control custom-checkbox checkbox-xl">
                    <input name="needs_repair_display" type="checkbox" class="custom-control-input" id="needs_repair_display">
                    <label class="custom-control-label" for="needs_repair_display">Display</label>
                </div>

                <div class="custom-control custom-checkbox checkbox-xl">
                    <input name="needs_repair_hinge" type="checkbox" class="custom-control-input" id="needs_repair_hinge">
                    <label class="custom-control-label" for="needs_repair_hinge">Hinge</label>
                </div>

                <div class="custom-control custom-checkbox checkbox-xl">
                    <input name="needs_repair_other" type="checkbox" class="custom-control-input" id="needs_repair_other">
                    <label class="custom-control-label" for="needs_repair_other">Other Damage</label>
                </div>

                <div class="container-fluid mt-5">
                    <div class="row">
                    <button class="btn btn-lg btn-primary ml-auto" style="width: 100%;" role="button" type="submit">Submit &raquo;</button>
                    </div>
                </div>
            </form>
        </div>
    </main>
</body>

</html>
