<?php
include('../auth/auth_check.php');

$config = parse_ini_file(realpath(__DIR__ . DIRECTORY_SEPARATOR . "../config.ini"), true);

$connection_string = $config['database']['type'] . ':host=' . $config['database']['hostname'] . ';dbname=' . $config['database']['database'];

try {
    $conn = new PDO($connection_string, $config['database']['username'], $config['database']['password']);
}catch (PDOException $e){
    print "Error!: " . $e->getMessage() . "<br/>";
}
